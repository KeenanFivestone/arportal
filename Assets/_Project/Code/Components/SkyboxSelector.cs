﻿using UnityEngine;

[RequireComponent (typeof (CanvasGroup))]
public class SkyboxSelector : MonoBehaviour
{
	public PortalController portal;
	private CanvasGroup canvasGroup;

	private void Awake ()
	{
		canvasGroup = GetComponent<CanvasGroup> ();
	}
	private void Update ()
	{
		if (portal.Exists)
		{
			canvasGroup.interactable = canvasGroup.blocksRaycasts = true;
			canvasGroup.alpha = 1f;
		}
		else
		{
			canvasGroup.interactable = canvasGroup.blocksRaycasts = false;
			canvasGroup.alpha = 0f;
		}
	}
}