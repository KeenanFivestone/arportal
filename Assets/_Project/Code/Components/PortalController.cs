﻿using UnityEngine;
using GoogleARCore;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
// Set up touch input propagation while using Instant Preview in the editor.
using Input = GoogleARCore.InstantPreviewInput;
#endif


public class PortalController : MonoBehaviour
{
	public GameObject portalGraphics;
	public Transform lookTarget;
	public Vector3 offset;

	public Trackable portalSurface;

	public bool Exists { get { return portalGraphics.activeSelf; } }

	private void Start ()
	{
		HidePortal ();
	}
	private void Update ()
	{
		if (Input.touchCount > 0)
		{
			var touch = Input.touches[0];
			if (!EventSystem.current.IsPointerOverGameObject (touch.fingerId))
			{
				TrackableHit hit;
				var raycastFilter = TrackableHitFlags.PlaneWithinPolygon | TrackableHitFlags.FeaturePointWithSurfaceNormal;
				if (Frame.Raycast (touch.position.x, touch.position.y, raycastFilter, out hit))
				{
					portalSurface = hit.Trackable;
					portalGraphics.SetActive (true);
					transform.position = hit.Pose.position + offset;
					var lookRotation = Quaternion.LookRotation (lookTarget.position - transform.position).eulerAngles;
					lookRotation.x = 0f;
					lookRotation.z = 0f;
					transform.rotation = Quaternion.Euler (lookRotation);
				}
			}
		}

		if (portalSurface == null || portalSurface.TrackingState != TrackingState.Tracking)
			HidePortal ();
		else
			ShowPortal ();
	}

	private void OnGUI ()
	{
		GUILayout.Toggle (Exists, new GUIContent ("Portal Exists"));
	}

	public void HidePortal ()
	{
		portalGraphics.SetActive (false);
	}

	public void ShowPortal ()
	{
		portalGraphics.SetActive (true);
	}
}