﻿using UnityEngine;
using TMPro;

[RequireComponent (typeof (TextMeshProUGUI))]
public class SkyboxNameDisplay : MonoBehaviour
{
	public SkyboxController skybox;
	private TextMeshProUGUI text;

	private void Awake ()
	{
		text = GetComponent<TextMeshProUGUI> ();
		skybox.onSkyboxChange.AddListener (() => text.text = skybox.Skyboxes[skybox.SkyboxIndex].name);
	}
}