﻿using UnityEngine;
using TMPro;

[RequireComponent (typeof (TextMeshProUGUI))]
public class PromptController : MonoBehaviour
{
	public PortalController portal;

	private TextMeshProUGUI text;

	private void Awake ()
	{
		text = GetComponent<TextMeshProUGUI> ();
	}
	private void Update ()
	{
		text.alpha = portal.Exists ? 0f : 1f;
	}
}