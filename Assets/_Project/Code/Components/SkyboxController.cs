﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent (typeof (MeshRenderer))]
public class SkyboxController : MonoBehaviour
{
	public Transform Camera;
	public List<Texture> Skyboxes;

	public UnityEvent onSkyboxChange;

	private MeshRenderer meshRenderer;
	private Material material;

	public int SkyboxIndex;

	private void Awake ()
	{
		meshRenderer = GetComponent<MeshRenderer> ();
		material = meshRenderer.sharedMaterial = Instantiate (meshRenderer.sharedMaterial);
	}
	private void Update ()
	{
		transform.position = Camera.position;
	}

	public void SetSkybox (int index)
	{
		SkyboxIndex = index % (Skyboxes.Count);
		material.SetTexture ("_MainTex", Skyboxes[SkyboxIndex]);
		onSkyboxChange.Invoke ();
	}

	public void NextSkybox ()
	{
		SetSkybox (SkyboxIndex + 1);
	}
	public void PreviousSkybox ()
	{
		SetSkybox (SkyboxIndex - 1);
	}
}